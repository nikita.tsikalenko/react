import React, {useEffect, useState} from "react";
import {Route, Routes} from "react-router-dom";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";
import Goods from "./pages/Goods";
import Modal from "./components/Modal/Modal";
import Main from "./pages/Main";
import {getAllGoods} from "./api/api";

function App() {
    const [cartCount, setCartCount] = useState(localStorage.getItem('cart') ? localStorage.getItem('cart').split(',').length : 0);
    const [favoriteCount, setFavoriteCount] = useState(localStorage.getItem('favorite') ? localStorage.getItem('favorite').split(',').length : 0);
    const [isRenderModal, setIsRenderModal] = useState(false);
    const [favorite, setFavorite] = useState(localStorage.getItem('favorite') ? localStorage.getItem('favorite').split(',') : [])
    const [cart, setCart] = useState(localStorage.getItem('cart') ? localStorage.getItem('cart').split(',') : []);
    const [header, setHeader] = useState();
    const [closeButton, setCloseButton] = useState();
    const [text, setText] = useState();
    const [actions, setActions] = useState();
    const [goods, setGoods] = useState();

    useEffect(() => {
            (async () => {
                setGoods(await getAllGoods());
            })()
        }, []
    )


    const closeModal = () => {
        setIsRenderModal(false)
    }

    const createModal = ({header, closeButton, text, actions}) => {
        setIsRenderModal(true);
        setHeader(header);
        setCloseButton(closeButton);
        setText(text);
        setActions(actions);
    }

    const changeFavorite = (article) => {
        if (favorite.includes(article)) {
            const index = favorite.indexOf(article);
            favorite.splice(index, 1);
        } else {
            favorite.push(article);
        }
        setFavorite(favorite);
        setFavoriteCount(favorite.length)
        localStorage.setItem('favorite', favorite.join(','));
    }

    const addToCart = (article) => {
        const newCart = cart;
        if (!newCart.includes(article)) {
            newCart.push(article);
            setCart(newCart);
            setCartCount(newCart.length)
            localStorage.setItem('cart', newCart.join(','));
        }

    }

    const removeFromCart = (article) => {
        const newCart = cart.filter(item => {
            return item !== article
        });
        setCart(newCart);
        setCartCount(newCart.length)
        localStorage.setItem('cart', newCart.join(','));
    }

    return (<>
            <Routes>
                <Route exact path='/' element={<Main cartCount={cartCount} favoriteCount={favoriteCount}/>}>
                    <Route index element={
                        goods ? <Goods goods={goods}
                                       favorite={favorite}
                                       changeFavorite={changeFavorite}
                                       createModal={createModal}
                                       closeModal={closeModal}
                                       addToCart={addToCart}
                        /> : <h2>Goods is loading...</h2>}/>
                    <Route path='cart' element={
                        goods ? <Cart goods={goods}
                                      favorite={favorite}
                                      cart={cart}
                                      changeFavorite={changeFavorite}
                                      createModal={createModal}
                                      closeModal={closeModal}
                                      removeFromCart={removeFromCart}
                        /> : <h2>Goods is loading...</h2>}/>
                    <Route path='favorite' element={
                        goods ? <Favorite goods={goods}
                                          favorite={favorite}
                                          changeFavorite={changeFavorite}
                                          createModal={createModal}
                                          closeModal={closeModal}
                                          addToCart={addToCart}
                        /> : <h2>Goods is loading...</h2>}
                    />
                    <Route path="*" element={<><h2>404</h2></>}/>
                </Route>
            </Routes>
            {isRenderModal && <Modal
                header={header}
                closeButton={closeButton}
                text={text}
                actions={actions}
                closeModal={closeModal}
            />
            }
        </>
    );
}

export default App;