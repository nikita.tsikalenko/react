import React from "react";
import GoodsList from "../../components/GoodsList/GoodsList";
import Button from "../../components/Button/Button";

function Cart(props) {
    const goodsInCart = props.cart.map(item => props.goods.filter(goods => goods.article === item)[0])
    return <>
        <GoodsList title='Cart'
                   goods={goodsInCart}
                   favorite={props.favorite}
                   changeFavorite={props.changeFavorite}
                   createModal={props.createModal}
                   closeModal={props.closeModal}
                   btnText='Remove'
                   action={
                       article => props.createModal({
                           header: 'Remove from card?',
                           closeButton: true,
                           text: 'Remove this goods from cart?',
                           actions: <Button backgroundColor={'#ff0000'} content={'Remove'} onClick={() => {
                               props.removeFromCart(article);
                               props.closeModal();
                           }}/>
                       })
                   }
        />
    </>
}

export default Cart;