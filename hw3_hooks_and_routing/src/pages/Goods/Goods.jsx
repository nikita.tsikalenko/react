import React from "react";
import GoodsList from "../../components/GoodsList/GoodsList";
import Button from "../../components/Button/Button";

function Goods(props) {
    return <>
        <GoodsList title='All Goods'
                   goods={props.goods}
                   favorite={props.favorite}
                   changeFavorite={props.changeFavorite}
                   createModal={props.createModal}
                   closeModal={props.closeModal}
                   btnText='Add to cart'
                   action={
                       article => props.createModal({
                           header: 'Add to card?',
                           closeButton: true,
                           text: 'Add this goods to cart?',
                           actions: <Button backgroundColor={'#00ff00'} content={'Add'} onClick={() => {
                               props.addToCart(article);
                               props.closeModal();
                           }}/>
                       })
                   }
        />
    </>
}

export default Goods;