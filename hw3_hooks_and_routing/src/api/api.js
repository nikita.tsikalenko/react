import axios from "axios";

export async function getAllGoods() {
    try {
        const {data} = await axios.get('/goods.json');
        return data;
    } catch (e) {
        console.log(e);
        return [];
    }
}
