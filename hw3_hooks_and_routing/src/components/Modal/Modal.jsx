import React, {useState} from "react";
import PropTypes from "prop-types";

function Modal(props) {
    const {header, closeButton, text, actions, closeModal} = props;
    const [isRender, setIsRender] = useState(true);

    return isRender && <div className={'modal'} onClick={(event) => {
        if (event.target.classList.contains('modal') || event.target.classList.contains('modal__cross')) {
            closeModal();
        }
    }
    }>
        <div className={'modal__body'}>
            <div className={'modal__header'}>
                <h2 className={'modal__title'}>
                    {header}
                </h2>
                {closeButton && <div className={'modal__cross'}>X</div>}
            </div>

            <p className={'modal__text'}>{text}</p>
            <div className={'modal__actions'}>
                {actions}
            </div>
        </div>
    </div>
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.element.isRequired,
    closeModal: PropTypes.func.isRequired,
}

export default Modal;

import './modal.scss'