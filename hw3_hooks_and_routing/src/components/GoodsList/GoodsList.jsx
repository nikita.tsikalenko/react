import React, {useState, useEffect} from "react";
import GoodsItem from "../GoodItem/GoodsItem";
import Button from "../Button/Button";
import PropTypes from 'prop-types';

function GoodsList(props) {
    const [showItem, setShowItem] = useState(setShowItemLength());
    const [favorite, setFavorite] = useState(props.favorite);

    useEffect(() => setShowItem(setShowItemLength()), [props.goods]);

    function setShowItemLength() {
        return props.goods.length < 3 ? props.goods.length < 2 ? [0] : [0, 1] : [0, 1, 2];
    }

    const makeGoodsList = () => {
        return props.goods && props.goods.length >= showItem.length && showItem.map((value) => {
                const {article} = props.goods[value];
                return <GoodsItem
                    key={article}
                    item={props.goods[value]}
                    isFavorite={favorite.includes(article)}
                    changeFavorite={props.changeFavorite}
                    createModal={props.createModal}
                    closeModal={props.closeModal}
                    btnText={props.btnText}
                    action={article => props.action(article)}
                />
            }
        )
    }

    const slideList = (slide) => {
        const newShowItem = showItem.map(index => {
            let newIndex = index + slide;
            if (newIndex < 0) {
                newIndex = props.goods.length + newIndex;
            }
            if (newIndex > props.goods.length - 1) {
                newIndex -= props.goods.length;
            }
            return newIndex;
        });
        setShowItem(newShowItem);
    }

    const goodsList = props.goods.length !== 0 ? makeGoodsList() : null;

    return <div className={'goods'}>
        <div className="goods__header">
            <h2 className='goods__title'>{props.title}</h2>
            {props.goods.length > showItem.length && <div className="goods__btns">
                <Button className="btn btn--arrow" content={'⯇'} onClick={() => slideList(-3)}/>
                <Button className="btn btn--arrow" content={'⯈'} onClick={() => slideList(3)}/>
            </div>}
        </div>
        <ul className="goods__list">
            {goodsList}
        </ul>
    </div>

}


GoodsList.propType = {
    favorite: PropTypes.array.isRequired,
    changeFavorite: PropTypes.func.isRequired,
    createModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

export default GoodsList;

import './goodsList.scss';