import React, {useState} from "react";
import Button from "../Button/Button";
import PropTypes from 'prop-types';

function GoodsItem(props) {
    const [item, setItem] = useState(props.item);
    const [isFavorite, setIsFavorite] = useState(props.isFavorite);

    return <>
        <li className="item">
            <img src={item.url} alt={item.name} className="item__img" width={230} height={230}/>
            <div className="item__body">
                <h3 className="item__title">{item.name}</h3>
                <p className="item__article">Article: {item.article}</p>
                <p className="item__price">{item.price}$</p>
            </div>
            <Button backgroundColor={'#333333'} content={props.btnText} onClick={() => {
                props.action(item.article);
            }}/>
            <Button
                className={isFavorite ? 'btn btn--favorite btn--yellow' : 'btn btn--favorite'}
                content={
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                        <path
                            d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>
                    </svg>}
                onClick={() => {
                    setIsFavorite(!isFavorite);
                    props.changeFavorite(item.article)
                }}/>
        </li>
    </>
}

GoodsItem.propType = {
    item: PropTypes.object.isRequired,
    isFavorite: PropTypes.bool.isRequired,
    changeFavorite: PropTypes.func.isRequired,
    createModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    btnText: PropTypes.string,
    action: PropTypes.func.isRequired,
}

export default GoodsItem;

import './goodsItem.scss';