import React from "react";
import Modal from "./components/Modal/Modal";
import GoodsList from "./components/GoodsList/GoodsList";
import Header from "./components/Header/Header";

class App extends React.Component {
    state = {
        isRenderModal: false,
        favorite: localStorage.getItem('favorite') ? localStorage.getItem('favorite').split(',') : [],
        cart: localStorage.getItem('cart') ? localStorage.getItem('cart').split(',') : [],
    }

    closeModal = () => {
        this.setState({
            isRenderModal: false,
        })
    }

    crateModal = ({header, closeButton, text, actions}) => {
        this.setState({
            isRenderModal: true,
            header: header,
            closeButton: closeButton,
            text: text,
            actions: actions,
        })
    }

    changeFavorite = (article) => {
        const {favorite} = this.state;
        if (favorite.includes(article)) {
            const index = favorite.indexOf(article);
            favorite.splice(index, 1);
        } else {
            favorite.push(article);
        }
        this.setState({favorite: favorite});
        localStorage.setItem('favorite', favorite.join(','));
    }

    addToCart = (article) => {
        const {cart} = this.state;
        cart.push(article);
        this.setState({cart: cart});
        localStorage.setItem('cart', cart.join(','));
    }

    render() {
        const {isRenderModal, header, closeButton, text, actions, favorite} = this.state;
        return <>
            <Header/>
            <GoodsList modal={this.crateModal}
                       favorite={favorite}
                       changeFavorite={this.changeFavorite}
                       crateModal={this.crateModal}
                       closeModal={this.closeModal}
                       addToCart={this.addToCart}
            />

            {isRenderModal && <Modal
                header={header}
                closeButton={closeButton}
                text={text}
                actions={actions}
                closeModal={this.closeModal}
            />}
        </>
    }
}

export default App;