import React, {Component} from "react";
import GoodsItem from "../GoodItem/GoodsItem";
import Button from "../Button/Button";
import {getAllGoods} from "../../api/api";
import PropTypes from 'prop-types';

class GoodsList extends Component {
    state = {
        showItem: [0, 1, 2],
        favorite: this.props.favorite,
        changeFavorite: this.props.changeFavorite,
        crateModal: this.props.crateModal,
        closeModal: this.props.closeModal,
        addToCart: this.props.addToCart,
    }

    componentDidMount() {
        (async()=> {
            this.setState({goods: await getAllGoods()});
        }
        )()
    }

    makeGoodsList = () => {
        const {showItem, goods, favorite, changeFavorite, crateModal, closeModal, addToCart} = this.state;
        return goods && showItem.map((value) => {
                const {article} = goods[value];
                return <GoodsItem
                    key={article}
                    item={goods[value]}
                    isFavorite={favorite.includes(article)}
                    changeFavorite={changeFavorite}
                    crateModal={crateModal}
                    closeModal={closeModal}
                    addToCart={addToCart}
                />
            }
        )
    }

    slideList(slide) {
        const {goods, showItem} = this.state;
        const newShowItem = showItem.map(index => {
            let newIndex = index + slide;
            if (newIndex < 0) {
                newIndex = goods.length + newIndex;
            }
            if (newIndex > goods.length - 1) {
                newIndex -= goods.length;
            }
            return newIndex;
        });
        this.setState({
            showItem: newShowItem,
        })
    }

    render() {
        const goodsList = this.makeGoodsList();
        return <div className={'goods'}>
            <div className="goods__header">
                <h2 className='goods__title'>All goods</h2>
                <div className="goods__btns">
                    <Button className="btn btn--arrow" content={'⯇'} onClick={() => this.slideList(-3)}/>
                    <Button className="btn btn--arrow" content={'⯈'} onClick={() => this.slideList(3)}/>
                </div>
            </div>
            <ul className="goods__list">
                {goodsList}
            </ul>
        </div>
    }
}

GoodsList.propType = {
    favorite: PropTypes.array.isRequired,
    changeFavorite: PropTypes.func.isRequired,
    crateModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

export default GoodsList;

import './goodsList.scss';