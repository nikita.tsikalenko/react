import React, {Component} from "react";
import Button from "../Button/Button";
import PropTypes from 'prop-types';

class GoodsItem extends Component {
    state = {
        item: this.props.item,
        isFavorite: this.props.isFavorite,
        changeFavorite: this.props.changeFavorite,
        crateModal: this.props.crateModal,
        closeModal: this.props.closeModal,
        addToCart: this.props.addToCart,
    }


    render() {
        const {item, isFavorite, changeFavorite, crateModal, closeModal, addToCart} = this.state;
        return <>
            <li className="item">
                <img src={item.url} alt={item.name} className="item__img" width={230} height={230}/>
                <div className="item__body">
                    <h3 className="item__title">{item.name}</h3>
                    <p className="item__article">Article: {item.article}</p>
                    <p className="item__price">{item.price}$</p>
                </div>
                <Button backgroundColor={'#333333'} content={'Add to cart'} onClick={() => {
                    crateModal({
                        header: 'Add to card?',
                        closeButton: true,
                        text: 'Add this goods to cart?',
                        actions: <Button backgroundColor={'#00ff00'} content={'Add'} onClick={() => {
                            addToCart(item.article);
                            closeModal();
                        }}/>

                    });
                }}/>
                <Button
                    className={isFavorite ? 'btn btn--favorite btn--yellow' : 'btn btn--favorite'}
                    content={
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                            <path
                                d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>
                        </svg>}
                    onClick={() => {
                        this.setState({isFavorite: !isFavorite})
                        changeFavorite(item.article)
                    }}/>
            </li>
        </>

    }
}

GoodsItem.propType = {
    item: PropTypes.object.isRequired,
    isFavorite: PropTypes.bool.isRequired,
    changeFavorite: PropTypes.func.isRequired,
    crateModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

export default GoodsItem;

import './goodsItem.scss';