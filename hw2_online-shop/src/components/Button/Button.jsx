import React from "react";
import PropTypes, {} from "prop-types";

class Button extends React.Component {
    render() {
        const {backgroundColor: bgc, content, onClick: onClickFunction, className} = this.props;
        const style = {backgroundColor: bgc}
        return <button
            className={className}
            style={style}
            onClick={onClickFunction}
        >
            {content}
        </button>
    }
}

Button.defaultProps = {
    className: 'btn',
    backgroundColor: '',
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string,
}

export default Button;

import './button.scss'