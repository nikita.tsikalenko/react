import React, {useEffect} from "react";

import {Route, Routes} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getAllGoods} from "./api/api";
import {setGoods} from "./store/actions/goodsActions";


import Goods from "./pages/Goods";
import Main from "./pages/Main";
import Modal from "./components/Modal/Modal";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";

function App() {
    const dispatch = useDispatch();

    const goods = useSelector(state => state.goods.list);
    const favorite = useSelector(state => state.favorite.list);
    const cart = useSelector(state => state.cart.list);
    const isRenderModal = useSelector(state => state.modal.isRenderModal);
    const modalHeader = useSelector(state => state.modal.modalHeader);
    const modalCloseButton = useSelector(state => state.modal.modalCloseButton);
    const modalText = useSelector(state => state.modal.modalText);
    const modalActions = useSelector(state => state.modal.modalActions);

    useEffect(() => {
            (async () => {
                dispatch(setGoods(await getAllGoods()));
            })()
        }, []
    )

    useEffect(() => {
        const onUnload = () => {
            localStorage.setItem('favorite', JSON.stringify(favorite));
            localStorage.setItem('cart', JSON.stringify(cart));
        }

        window.addEventListener("beforeunload", onUnload);

        return () => window.removeEventListener("beforeunload", onUnload)
    })

    return (<>
            <Routes>
                <Route exact path='/' element={<Main/>}>
                    <Route index element={
                        goods ? <Goods/> : <h2>Goods is loading...</h2>}
                    />
                    <Route path='cart' element={
                        goods ? <Cart/> : <h2>Cart is loading...</h2>}
                    />
                    <Route path='favorite' element={
                        goods ? <Favorite/> : <h2>Favorite is loading...</h2>}
                    />
                    <Route path="*" element={<><h2>404</h2></>}/>
                </Route>
            </Routes>
            {isRenderModal && <Modal
                header={modalHeader}
                closeButton={modalCloseButton}
                text={modalText}
                actions={modalActions}
            />
            }
        </>
    );
}

export default App;