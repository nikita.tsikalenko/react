import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";

import {setArrItemToRender, setLengthArrItemToRender} from "../../store/actions/itemToRenderActions";

import GoodsItem from "../GoodItem";

import Button from "../Button/Button";
import './goodsList.scss';

function GoodsList({title, goods}) {
    const dispatch = useDispatch();
    const arrItemToRender = useSelector(state => state.itemToRender.arr);

    useEffect(() => {
        dispatch(setLengthArrItemToRender(goods))
    }, [goods]);

    const makeGoodsList = () => {
        return goods && goods.length >= arrItemToRender.length && arrItemToRender.map((value) => {
                if (goods[value]) {
                    const {article} = goods[value];
                    return <GoodsItem
                        key={article}
                        item={goods[value]}
                    />
                }
            }
        )
    }

    const slideList = (slide) => {
        const newArrItemToShow = arrItemToRender.map(index => {
            let newIndex = index + slide;
            if (newIndex < 0) {
                newIndex = goods.length + newIndex;
            }
            if (newIndex > goods.length - 1) {
                newIndex -= goods.length;
            }
            return newIndex;
        });
        dispatch(setArrItemToRender(newArrItemToShow));
    }

    return <div className={'goods'}>
        <div className="goods__header">
            <h2 className='goods__title'>{title}</h2>
            {goods.length > arrItemToRender.length && <div className="goods__btns">
                <Button className="btn btn--arrow" content={'⯇'} onClick={() => slideList(-3)}/>
                <Button className="btn btn--arrow" content={'⯈'} onClick={() => slideList(3)}/>
            </div>}
        </div>
        <ul className="goods__list">
            {goods.length !== 0 ? makeGoodsList() : null}
        </ul>
    </div>

}

export default GoodsList;