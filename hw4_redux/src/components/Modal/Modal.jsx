import React from "react";
import {useDispatch} from "react-redux";

import {closeModal} from "../../store/actions/modalActions";

import './modal.scss'

function Modal({header, closeButton, text, actions}) {
    const dispatch = useDispatch();

    return <div className={'modal'} onClick={
        (event) => {
            if (event.target.classList.contains('modal') || event.target.classList.contains('modal__cross')) {
                dispatch(closeModal());
            }
        }
    }>
        <div className={'modal__body'}>
            <div className={'modal__header'}>
                <h2 className={'modal__title'}>
                    {header}
                </h2>
                {closeButton && <div className={'modal__cross'}>X</div>}
            </div>

            <p className={'modal__text'}>{text}</p>
            <div className={'modal__actions'}>
                {actions}
            </div>
        </div>
    </div>
}

export default Modal;