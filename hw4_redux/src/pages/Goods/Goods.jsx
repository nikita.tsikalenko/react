import React from "react";
import GoodsList from "../../components/GoodsList";
import {useSelector} from "react-redux";

function Goods() {
    const goods = useSelector(state => state.goods.list);
    return <>
        <GoodsList
            title='All Goods'
            goods={goods}
        />
    </>
}


export default Goods;