import React from "react";
import {Outlet} from "react-router-dom";
import Header from "../../components/Header/Header";

function Main(props) {
    return <>
        <Header cartCount={props.cartCount} favoriteCount={props.favoriteCount}/>
        <main className={'main'}>
            <Outlet/>
        </main>
    </>
}

export default Main;