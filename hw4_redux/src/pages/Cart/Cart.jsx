import React from "react";
import GoodsList from "../../components/GoodsList";
import {useSelector} from "react-redux";

function Cart() {
    const cart = useSelector(state => state.cart.list)
    return <>
        <GoodsList title='Cart'
                   goods={cart}
        />
    </>
}

export default Cart;