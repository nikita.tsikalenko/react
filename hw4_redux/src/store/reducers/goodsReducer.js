const initialState = {
    list: [],
}

const goodsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_GOODS': {
            return {...state, list: action.payload}
        }
        default:
            return state;
    }
}

export default goodsReducer;