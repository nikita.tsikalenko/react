const initialState = {
    isRenderModal: false,
    modalHeader: null,
    modalCloseButton: null,
    modalText: null,
    modalActions: null,
}

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TO_CART_MODAL': {
            return {...state, isRenderModal: true, ...action.payload}
        }
        case 'REMOVE_FROM_CART_MODAL': {
            return {...state, isRenderModal: true, ...action.payload}
        }
        case 'CLOSE_MODAL': {
            return {...state, isRenderModal: false}
        }
        default:
            return state;
    }
}

export default modalReducer;