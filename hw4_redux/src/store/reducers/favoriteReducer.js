const initialState = {
    list: localStorage.getItem('favorite') ? JSON.parse(localStorage.getItem('favorite')) : [],
};

const favoriteReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_FAVORITE': {
            const newFavorite = state.list.some(item => action.payload.article === item.article) ? state.list.filter(item => item.article !== action.payload.article) : [...state.list, action.payload];
            return {...state, list: newFavorite}
        }
        default:
            return state;
    }
}

export default favoriteReducer;