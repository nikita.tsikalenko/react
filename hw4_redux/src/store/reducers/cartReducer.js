const initialState = {
    list: localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [],
}

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TO_CART': {
            const newCart = [...state.list, action.payload];
            return {...state, list: newCart}
        }
        case 'REMOVE_FROM_CART': {
            const newCart = state.list.filter(item => item.article !== action.payload.article);
            return {...state, list: newCart}
        }
        default:
            return state;
    }
}

export default cartReducer;