export const setArrItemToRender = (arr) => {
    return {
        type: 'SET_ARR_ITEM_TO_RENDER',
        payload: arr
    }
}

export const setLengthArrItemToRender = (itemList) => {
    return {
        type: 'SET_LENGTH_ARR_ITEM_TO_RENDER',
        payload: itemList
    }
}