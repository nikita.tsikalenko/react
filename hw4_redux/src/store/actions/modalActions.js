export const addToCartModal = (modalInfo) => {
    return {
        type: 'ADD_TO_CART_MODAL',
        payload: modalInfo,
    }
}

export const removeFromCartModal = (modalInfo) => {
    return {
        type: 'REMOVE_FROM_CART_MODAL',
        payload: modalInfo
    }
}

export const closeModal = () => {
    return {
        type: 'CLOSE_MODAL',
    }
}