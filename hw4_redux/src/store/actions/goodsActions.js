import {getAllGoods} from "../../api/api";

export const setGoods = () => async dispatch => {
    const response = await getAllGoods();
    if (response instanceof Error) {
        dispatch({
            type: 'GET_GOODS',
            payload: [],
        })
    } else {
        dispatch({
            type: 'GET_GOODS',
            payload: response,
        })
    }
}
