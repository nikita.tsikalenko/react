export const changeFavorite = (item) => {
    return {
        type: 'CHANGE_FAVORITE',
        payload: item,
    }
}