import {combineReducers} from "redux";
import cartReducer from "./cartReducer";
import favoriteReducer from "./favoriteReducer";
import modalReducer from "./modalReducer";
import itemToRenderReducer from "./itemToRenderReducer";
import goodsReducer from "./goodsReducer";

const rootReducer = combineReducers({
        cart: cartReducer,
        favorite: favoriteReducer,
        modal: modalReducer,
        itemToRender: itemToRenderReducer,
        goods: goodsReducer,
    })
    // switch (action.type) {
        // case 'GET_GOODS': {
        //     return {...state, goods: action.payload}
        // }
        // case 'ADD_TO_CART': {
        //     const newCart = [...state.cart, action.payload];
        //     return {...state, cart: newCart, cartCount: newCart.length}
        // }
        // case 'REMOVE_FROM_CART': {
        //     const newCart = state.cart.filter(item => item.article !== action.payload.article);
        //     return {...state, cart: newCart, cartCount: newCart.length}
        // }
        // case 'CHANGE_FAVORITE': {
        //     const newFavorite = state.favorite.some(item => action.payload.article === item.article) ? state.favorite.filter(item => item.article !== action.payload.article) : [...state.favorite, action.payload];
        //     return {...state, favorite: newFavorite, favoriteCount: newFavorite.length}
        // }
        // case 'ADD_TO_CART_MODAL': {
        //     return {...state, isRenderModal: true, ...action.payload}
        // }
        // case 'REMOVE_FROM_CART_MODAL': {
        //     return {...state, isRenderModal: true, ...action.payload}
        // }
        // case 'CLOSE_MODAL': {
        //     return {...state, isRenderModal: false}
        // }
        // case 'SET_ARR_ITEM_TO_RENDER': {
        //     return {...state, arrItemToRender: action.payload}
        // }
        // case 'SET_LENGTH_ARR_ITEM_TO_RENDER': {
        //     return {
        //         ...state,
        //         arrItemToRender: makeArrItemToRender(action.payload, 3)
        //     }
        // }
    //     default:
    //         return state;
    // }

export default rootReducer;