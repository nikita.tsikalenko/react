const initialState = {
    arr: [],
}

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_ARR_ITEM_TO_RENDER': {
            return {...state, arr: action.payload}
        }
        case 'SET_LENGTH_ARR_ITEM_TO_RENDER': {
            return {
                ...state,
                arr: makeArrItemToRender(action.payload, 3)
            }
        }
        default:
            return state;
    }
}

function makeArrItemToRender(listOfGoods, maxCount) {
    const newArr = []
    for (let i = 0; i < listOfGoods.length; i++) {
        if (i === maxCount){
            break;
        }
        newArr.push(i);
    }
    return newArr;
}

export default modalReducer;