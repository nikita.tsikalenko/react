import React from "react";
import GoodsList from "../../components/GoodsList";
import {useSelector} from "react-redux";
import Checkout from "../../components/Checkout";

function Cart() {
    const cart = useSelector(state => state.cart.list);
    return <>
        <GoodsList title='Cart'
                   goods={cart}
        />

        {cart.length !== 0 && <Checkout/>}
    </>
}

export default Cart;