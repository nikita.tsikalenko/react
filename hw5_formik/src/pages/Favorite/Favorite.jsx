import React from "react";
import GoodsList from "../../components/GoodsList";
import {useSelector} from "react-redux";

function Favorite() {
    const favorite = useSelector(state => state.favorite.list);

    return <>
        <GoodsList title='Favorite'
                   goods={favorite}
        />
    </>
}

export default Favorite;