import React from "react";
import {Field, Form, Formik} from "formik";
import * as Yup from 'yup'

import './checkout.scss';
import '../Button/button.scss'
import FormatPhoneField from "../FormatPhoneField";
import {useDispatch} from "react-redux";
import {checkout} from "../../store/actions/cartActions";

function Checkout() {
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(checkout(values))

    }

    const SignupSchema = Yup.object().shape({
        firstName: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        lastName: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        age: Yup.number()
            .min(0)
            .max(99)
            .integer()
            .positive()
            .required('Required'),
        address: Yup.string()
            .min(10, 'Too Short!')
            .max(100, 'Too Long!')
            .required('Required'),
        phone: Yup.string()
            .min(17, 'Too Short!')
            .max(19, 'Too Long!')
            .required('Required'),
    });

    return <Formik
        initialValues={{
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phone: ''
        }}
        onSubmit={(values) => {
            handleSubmit(values);
        }}
        validationSchema={SignupSchema}
    >
        {({errors, touched}) => (
            <Form className='checkout'>
                <h2 className='checkout__title'>Checkout</h2>
                <div>
                    <label htmlFor="firstName">First name:</label>
                    <Field className='checkout__field' id='firstName' name='firstName' type='text'/>
                    {errors.firstName && touched.firstName ? (
                        <div className='checkout__error'>{errors.firstName}</div>
                    ) : null}
                </div>
                <div>
                    <label htmlFor="firstName">Last name:</label>
                    <Field className='checkout__field' id='lastName' name='lastName' type='text'/>
                    {errors.lastName && touched.lastName ? (
                        <div className='checkout__error'>{errors.lastName}</div>
                    ) : null}
                </div>
                <div>
                    <label htmlFor="firstName">Age:</label>
                    <Field className='checkout__field' id='age' name='age' type='number'/>
                    {errors.age && touched.age ? (
                        <div className='checkout__error'>{errors.age}</div>
                    ) : null}
                </div>
                <div>
                    <label htmlFor="firstName">Address:</label>
                    <Field className='checkout__field' id='address' name='address' type='text'/>
                    {errors.address && touched.address ? (
                        <div className='checkout__error'>{errors.address}</div>
                    ) : null}
                </div>
                <div>
                    <label htmlFor="firstName">Phone:</label>
                    <FormatPhoneField className='checkout__field' id='phone' name='phone'
                                      type='text'></FormatPhoneField>
                    {errors.phone && touched.phone ? (
                        <div className='checkout__error'>{errors.phone}</div>
                    ) : null}
                </div>
                <button className='btn btn--gray' type='submit'>Checkout</button>
            </Form>
        )
        }
    </Formik>
}

export default Checkout;