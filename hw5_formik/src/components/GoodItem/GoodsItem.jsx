import React from "react";
import {useDispatch, useSelector} from "react-redux";

import {addToCartModal, closeModal, removeFromCartModal} from "../../store/actions/modalActions";
import {addToCart, removeFromCart} from "../../store/actions/cartActions";
import {changeFavorite} from "../../store/actions/favoriteActions";

import Button from "../Button";

import './goodsItem.scss';

function GoodsItem({item}) {
    const dispatch = useDispatch();
    const favorite = useSelector(state => state.favorite.list)
    const cart = useSelector(state => state.cart.list)

    const isInCart = cart.some(cartItem => cartItem.article === item.article);
    const isFavorite = favorite.some(favItem => favItem.article === item.article);

    return <>
        <li className="item">
            <img src={item.url} alt={item.name} className="item__img" width={230} height={230}/>
            <div className="item__body">
                <h3 className="item__title">{item.name}</h3>
                <p className="item__article">Article: {item.article}</p>
                <p className="item__price">{item.price}$</p>
            </div>
            <Button backgroundColor={'#333333'} content={isInCart ? 'Remove from cart' : 'Add to cart'} onClick={() => {
                isInCart ? dispatch(removeFromCartModal({
                    modalHeader: 'Remove from card?',
                    modalCloseButton: true,
                    modalText: 'Remove this goods from cart?',
                    modalActions: <Button backgroundColor={'#ff0000'} content={'Remove'} onClick={() => {
                        dispatch(removeFromCart(item))
                        dispatch(closeModal());
                    }}/>
                })) : dispatch(addToCartModal({
                    modalHeader: 'Add to card?',
                    modalText: 'Add this goods to cart?',
                    modalCloseButton: true,
                    modalActions: <Button backgroundColor={'#00ff00'} content={'Add'} onClick={() => {
                        dispatch(addToCart(item));
                        dispatch(closeModal());
                    }}/>
                }))
            }}/>
            <Button
                className={isFavorite ? 'btn btn--favorite btn--yellow' : 'btn btn--favorite'}
                content={
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                        <path
                            d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>
                    </svg>}
                onClick={() => {
                    dispatch(changeFavorite(item));
                }}/>
        </li>
    </>
}

export default GoodsItem;