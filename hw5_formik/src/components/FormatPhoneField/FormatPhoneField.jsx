import React from "react";
import {useField} from 'formik'
import NumberFormat from "react-number-format";

function FormatPhoneField(props) {
    const {name, className} = props
    const [field] = useField(name)
    return <NumberFormat {...field} className={className} allowEmptyFormatting format="+38 (###) ###-##-##" mask="_"/>
}

export default FormatPhoneField;