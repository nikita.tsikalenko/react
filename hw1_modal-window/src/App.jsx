import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            isRenderModal: false,
        }
    }
    closeModal(){
        this.setState({
            isRenderModal: false,
        })
    }
    render() {
        const {isRenderModal, header, closeButton, text, actions} = this.state;

        return <>
            <Button backgroundColor={'red'} text={'Delete'} onClick={() => {
                this.crateModal({
                    isRenderModal: true,
                    header: 'Do you want to delete this file?',
                    closeButton: true,
                    text: 'Once you delete this file, it won’t be possible to undo this action. \n' +
                        'Are you sure you want to delete it?',
                    actions: <>
                        <Button backgroundColor={'darkred'} text={'Ok'}/>
                        <Button backgroundColor={'darkred'} text={'Delete'}/>
                    </>
                })
            }}/>

            <Button backgroundColor={'green'} text={'Create'} onClick={() => {
                this.crateModal({
                    isRenderModal: true,
                    header: 'Qwerty',
                    closeButton: false,
                    text: 'Test',
                    actions: <>
                        <Button backgroundColor={'green'} text={'Ok'}/>
                    </>
                })
            }}
            />

            {isRenderModal && <Modal
                header={header}
                closeButton={closeButton}
                text={text}
                actions={actions}
                closeModal={this.closeModal.bind(this)}
            />}
        </>
    }

    crateModal({header, closeButton, text, actions}) {
        this.setState({
            isRenderModal: true,
            header: header,
            closeButton: closeButton,
            text: text,
            actions: actions,
        })
    }
}

export default App;
