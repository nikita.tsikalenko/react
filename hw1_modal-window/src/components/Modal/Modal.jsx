import React from "react";
import './modal.scss'

class Modal extends React.Component {
    constructor() {
        super();
        this.state = {
            isRender: true,
        }
    }

    render() {
        const {header, closeButton, text, actions, closeModal} = this.props;

        return this.state.isRender && <div className={'modal'} onClick={(event) => {
                if (event.target.classList.contains('modal') || event.target.classList.contains('modal__cross')) {
                    closeModal();
                }
            }
        }>
            <div className={'modal__body'}>
                <div className={'modal__header'}>
                    <h2 className={'modal__title'}>
                        {header}
                    </h2>
                    {closeButton && <div className={'modal__cross'}>X</div>}
                </div>

                <p className={'modal__text'}>{text}</p>
                <div className={'modal__actions'}>
                    {actions}
                </div>
            </div>
        </div>
    }
}

export default Modal;