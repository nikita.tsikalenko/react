import React from "react";
import './button.scss'

class Button extends React.Component{
    render() {
        const {backgroundColor: bgc, text, onClick: onClickFunction} = this.props;
        const style = {backgroundColor: bgc}
        return <button className={'btn'} style={style} onClick={onClickFunction}>{text}</button>
    }
}

export default Button;